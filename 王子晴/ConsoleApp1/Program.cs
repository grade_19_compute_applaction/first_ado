﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;    //对Sql Server进行操作的数据访问类

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string connnectionString = "server=.;database=Krystal;uid=sa;pwd=wzq0307";   // .是指当前数据库服务器；database--数据库名；uid--用户名；pwd--密码

            SqlConnection sqlConnection = new SqlConnection(connnectionString);    //连接，定义一个connection；构造函数是一种方法、函数，会特意用来创造、实例化对象的方法

            sqlConnection.Open();    //Open就是打开到当前数据库的连接

            var SqlCommandString = "select * from student";   //查询单独拿出来，变成变量

            SqlCommand sqlCommand = new SqlCommand(SqlCommandString, sqlConnection);

            //或者 SqlCommand sqlCommand = new SqlCommand("select * from student",sqlConnection);

            var sdr = sqlCommand.ExecuteReader();

            Console.WriteLine("{0}\t{1}\t{2}", "编号", "学生姓名", "班级编号");

            while (sdr.Read()) {

                Console.WriteLine("{0}\t{1}\t{2}", sdr["studentId"], sdr["studentName"], sdr["classId"]);

            }

            sqlConnection.Close();
        }
    }
}
