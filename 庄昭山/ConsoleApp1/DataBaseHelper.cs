﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
      public  class DataBaseHelper
    {
        public static void PrintUser()
        {
            string connectionString = "server=.;database=dbtest;uid=sa;pwd=123wan520";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();

            var sqlCommandString = "select * from stuInfo";

            SqlCommand sqlCommand = new SqlCommand(sqlCommandString, sqlConnection);

            var sdr = sqlCommand.ExecuteReader();


            Console.WriteLine("{0}\t{1}\t{2}\t{3}", "stuId", "名字", "年龄", "性别");
            while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}", sdr["stuId"], sdr["stuName"], sdr["stuAge"], sdr["stuSex"]);
            }

            sqlConnection.Close();

        }
        public void PrintUserInfoAdaper()
        {
            string con = "server=.;database=dbtest;uid=sa;pwd=123wan520";

            SqlConnection sqlCon = new SqlConnection(con);

            sqlCon.Open();

            var sqlCom = "select * from stuInfo";

            SqlDataAdapter sqlData = new SqlDataAdapter(sqlCom, con);

            DataTable data = new DataTable();

            sqlData.Fill(data);

            Console.WriteLine("{0}\t{1}\t{2}\t{3}", "stuId", "名字", "年龄", "性别");

            for (int i = 0; i < data.Rows.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}", data.Rows[i]["stuId"], data.Rows[i]["stuName"], data.Rows[i]["stuAge"], data.Rows[i]["stuSex"]);
            }
            sqlCon.Close();
        }
    }
}
