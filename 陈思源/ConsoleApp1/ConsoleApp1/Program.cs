﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //连接字符串
            string connStr = "server= .;database =Test;uid =sa;pwd =123456";
            //声明连接对象 
            SqlConnection conn = new SqlConnection(connStr);
            //打开连接
            conn.Open();
            //sql查询语句
            string sql = "select * from userInfo";
            //声明适配器对象
            SqlCommand cmd = new SqlCommand(sql,conn);
            //执行查询命令
            var line= cmd.ExecuteReader();
            //查询数据
            while (line.Read()) {
                Console.WriteLine("[0],[1],[2]",line["userID"],line["userName"],line["userSex"]);
            }
            //关闭连接
            conn.Close();
        }
    }
}
