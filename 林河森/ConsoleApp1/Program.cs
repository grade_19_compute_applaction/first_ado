﻿using System;
using System.Data.SqlClient;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //连接字符串
            string connStr = "server= .;database =Schools;uid =sa;pwd =123456;";
            //声明连接对象 
            SqlConnection connection = new SqlConnection(connStr);
            //打开连接
            connection.Open();
            //sql查询语句
            string sql = "SELECT * FROM StudentsId";
            //声明适配器对象
            SqlCommand cmd = new SqlCommand(sql,connection);
            //执行查询命令
            var line = cmd.ExecuteReader();
            //查询数据
            while (line.Read())
            {
                Console.WriteLine("[0],[1],[2],[3]", line["StudentsId"], line["StudentsName"], line["StudentsAge"],line["StudentsScore"]);
            }
            //关闭连接
            connection.Close();
        }
    }
}
