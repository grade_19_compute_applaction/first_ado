﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = "server =.; database =huan;uid = sa; pwd = 123456";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();

            var sqlCommandString = "select * from Student";
            SqlCommand sqlCommand = new SqlCommand(sqlCommandString, sqlConnection);

            var sdr = sqlCommand.ExecuteReader();

            while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}",sdr["stuId"],sdr["stuName"],sdr["stuAge"]);
            }
            sqlConnection.Close();
        }
    }
}
